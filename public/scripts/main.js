$(() => {
    // Variables
    let categories = undefined;
    let navElement = undefined;
    let navItems = undefined;
    let productElement = undefined;
    const navContainer = document.querySelector(".nav");
    const productsList = document.querySelector(".products-list");

    // Receive an object contaning an array of categories, iterate over it and for every array, create a nav element and append it
    // to the navigation menu.
    const populateNavBar = (categories) => {
        $.each(categories.items, ((key, value) => {
            navElement = `<a id="${value.id}" class="nav-link" href="#${value.path}">${value.name.toUpperCase()}</a>`;
            $(navContainer).append(navElement);
        }));

        navElement = `<a class="nav-link" href="#contact">CONTATO</a>`;
        $(navContainer).append(navElement);
    };

    // Receive an element from the navigation menu just clicked, containing it's id. Get the products list based on it,
    // create a product card and append it into the products list.
    const populateProductsList = (clickedElement) => {
        $.ajax({
            url: `http://localhost:8888/api/V1/categories/${clickedElement.target.id}`,
            type: 'get'
        }).done((products) => {
            $.each(products.items, ((key, value) => {
                productElement = `
                    <div class="card" id="${value.id}">
                        <img src="${value.image}" class="card-img-top" alt="${value.path}">
                        <h5 class="card-title">${value.name.toUpperCase()}</h5>
                        ${value.specialPrice ? `<span class="card-special"><del>R$ ${value.specialPrice}</span>` : "" }
                        <div class="card-bottom">
                            <p class="card-text font-red">R$ ${value.price}</p>
                            <button href="#" class="btn bg-green">COMPRAR</button>
                        </div>
                    </div>
                `;

                $(productsList).append(productElement);
            }));
        });
    };

    // Ran from the document.ready DOM event, will search on the server for the categories list, to populate
    // the navigation bar.
    $.ajax({
        url: "http://localhost:8888/api/V1/categories/list",
        type: "get"
    }).done((categories) => {
        populateNavBar(categories);       

        navItems = document.querySelectorAll(".nav-link");
        $(navItems).click((element) => {
            if (element.target.id) {
                $(productsList).html("");
                populateProductsList(element);
            }
        });
    });
});