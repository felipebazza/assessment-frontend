# Teste Frontend Felipe Bazzanella

## Instruções para rodar o projeto
- Navegar para a branch desafio
- Rodar a aplicação
```
npm start
```

## Observação
- Por um problema no servidor web foi utlizado Bootstrap, FontAwesome e JQuery via CDN, pois instalando via npm e tentando a importação dava erro MIME, retornando um HTML ao invés do arquivo. 